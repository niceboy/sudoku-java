package sudoku.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;

import org.junit.Test;

import sudoku.lib.Solver;

/**
 * Test cases for the {@link Solver#solve(int[][])} method.
 * 
 * @author Dennis Hedegaard
 * 
 */
public class SolverTest_solve {
	/**
	 * Check to see if we can solve a sudoku solvable by direct deduction. The
	 * sudoku can be found: <a
	 * href="http://projecteuler.net/index.php?section=problems&id=96"
	 * >http://projecteuler.net/index.php?section=problems&id=96</a>
	 */
	@Test
	public void testSolve_solve1() {
		int[][] expected = new int[][] { { 4, 8, 3, 9, 2, 1, 6, 5, 7 },
				{ 9, 6, 7, 3, 4, 5, 8, 2, 1 }, { 2, 5, 1, 8, 7, 6, 4, 9, 3 },
				{ 5, 4, 8, 1, 3, 2, 9, 7, 6 }, { 7, 2, 9, 5, 6, 4, 1, 3, 8 },
				{ 1, 3, 6, 7, 9, 8, 2, 4, 5 }, { 3, 7, 2, 6, 8, 9, 5, 1, 4 },
				{ 8, 1, 4, 2, 5, 3, 7, 6, 9 }, { 6, 9, 5, 4, 1, 7, 3, 8, 2 } };
		int[][] unsolved = new int[][] { { 0, 0, 3, 0, 2, 0, 6, 0, 0 },
				{ 9, 0, 0, 3, 0, 5, 0, 0, 1 }, { 0, 0, 1, 8, 0, 6, 4, 0, 0 },
				{ 0, 0, 8, 1, 0, 2, 9, 0, 0 }, { 7, 0, 0, 0, 0, 0, 0, 0, 8 },
				{ 0, 0, 6, 7, 0, 8, 2, 0, 0 }, { 0, 0, 2, 6, 0, 9, 5, 0, 0 },
				{ 8, 0, 0, 2, 0, 3, 0, 0, 9 }, { 0, 0, 5, 0, 1, 0, 3, 0, 0 } };
		int[][] result = Solver.solve(unsolved);
		// bad testing sollution since java doesn't implement a proper "equals"
		// for double arrays, we iterate the double array and compare the inner
		// arrays.
		for (int i = 0; i < result.length; i++)
			if (!Arrays.equals(expected[i], result[i]))
				fail("arrays not equal for y position " + i + ".");
		assertTrue(true);
	}

	/**
	 * Checks to see if an already solved sudoku ({@link #testSolve_solve1()})
	 * returns the same data.
	 */
	@Test
	public void testSolve_solve2() {
		int[][] expected = new int[][] { { 4, 8, 3, 9, 2, 1, 6, 5, 7 },
				{ 9, 6, 7, 3, 4, 5, 8, 2, 1 }, { 2, 5, 1, 8, 7, 6, 4, 9, 3 },
				{ 5, 4, 8, 1, 3, 2, 9, 7, 6 }, { 7, 2, 9, 5, 6, 4, 1, 3, 8 },
				{ 1, 3, 6, 7, 9, 8, 2, 4, 5 }, { 3, 7, 2, 6, 8, 9, 5, 1, 4 },
				{ 8, 1, 4, 2, 5, 3, 7, 6, 9 }, { 6, 9, 5, 4, 1, 7, 3, 8, 2 } };
		int[][] result = Solver.solve(expected);
		// bad testing sollution since java doesn't implement a proper "equals"
		// for double arrays, we iterate the double array and compare the inner
		// arrays.
		for (int i = 0; i < result.length; i++)
			if (!Arrays.equals(expected[i], result[i]))
				fail("arrays not equal for y position " + i + ".");
		assertTrue(true);
	}

	/**
	 * Check to see if we can solve a sudoku. The sudoku is the one found at <a
	 * href
	 * ="http://en.wikipedia.org/wiki/File:Sudoku-by-L2G-20050714.svg">http:/
	 * /en.wikipedia.org/wiki/File:Sudoku-by-L2G-20050714.svg</a>. Should end up
	 * being <a href=
	 * "http://en.wikipedia.org/wiki/File:Sudoku-by-L2G-20050714_solution.svg"
	 * >http
	 * ://en.wikipedia.org/wiki/File:Sudoku-by-L2G-20050714_solution.svg</a>.
	 * 
	 * @see <a
	 *      href="http://en.wikipedia.org/wiki/Sudoku">http://en.wikipedia.org/wiki/Sudoku</a>
	 */
	@Test
	public void testSolve_solve3() {
		int[][] expected = new int[][] { { 5, 3, 4, 6, 7, 8, 9, 1, 2 },
				{ 6, 7, 2, 1, 9, 5, 3, 4, 8 }, { 1, 9, 8, 3, 4, 2, 5, 6, 7 },
				{ 8, 5, 9, 7, 6, 1, 4, 2, 3 }, { 4, 2, 6, 8, 5, 3, 7, 9, 1 },
				{ 7, 1, 3, 9, 2, 4, 8, 5, 6 }, { 9, 6, 1, 5, 3, 7, 2, 8, 4 },
				{ 2, 8, 7, 4, 1, 9, 6, 3, 5 }, { 3, 4, 5, 2, 8, 6, 1, 7, 9 } };
		int[][] unsolved = new int[][] { { 5, 3, 0, 0, 7, 0, 0, 0, 0 },
				{ 6, 0, 0, 1, 9, 5, 0, 0, 0 }, { 0, 9, 8, 0, 0, 0, 0, 6, 0 },
				{ 8, 0, 0, 0, 6, 0, 0, 0, 3 }, { 4, 0, 0, 8, 0, 3, 0, 0, 1 },
				{ 7, 0, 0, 0, 2, 0, 0, 0, 6 }, { 0, 6, 0, 0, 0, 0, 2, 8, 0 },
				{ 0, 0, 0, 4, 1, 9, 0, 0, 5 }, { 0, 0, 0, 0, 8, 0, 0, 7, 9 } };
		int[][] result = Solver.solve(unsolved);
		// bad testing sollution since java doesn't implement a proper "equals"
		// for double arrays, we iterate the double array and compare the inner
		// arrays.
		for (int i = 0; i < result.length; i++)
			if (!Arrays.equals(expected[i], result[i]))
				fail("arrays not equal for y position " + i + ".");
		assertTrue(true);
	}

	/**
	 * Check to see if we can solve a sudoku. The sudoku can be found here <a
	 * href="http://www.websudoku.com/images/example-steps.html">http://www.
	 * websudoku.com/images/example-steps.html</a>.
	 */
	@Test
	public void testSolve_solve4() {
		int[][] expected = new int[][] { { 1, 7, 2, 5, 4, 9, 6, 8, 3 },
				{ 6, 4, 5, 8, 7, 3, 2, 1, 9 }, { 3, 8, 9, 2, 6, 1, 7, 4, 5 },
				{ 4, 9, 6, 3, 2, 7, 8, 5, 1 }, { 8, 1, 3, 4, 5, 6, 9, 7, 2 },
				{ 2, 5, 7, 1, 9, 8, 4, 3, 6 }, { 9, 6, 4, 7, 1, 5, 3, 2, 8 },
				{ 7, 3, 1, 6, 8, 2, 5, 9, 4 }, { 5, 2, 8, 9, 3, 4, 1, 6, 7 } };
		int[][] unsolved = new int[][] { { 0, 0, 0, 0, 0, 0, 6, 8, 0 },
				{ 0, 0, 0, 0, 7, 3, 0, 0, 9 }, { 3, 0, 9, 0, 0, 0, 0, 4, 5 },
				{ 4, 9, 0, 0, 0, 0, 0, 0, 0 }, { 8, 0, 3, 0, 5, 0, 9, 0, 2 },
				{ 0, 0, 0, 0, 0, 0, 0, 3, 6 }, { 9, 6, 0, 0, 0, 0, 3, 0, 8 },
				{ 7, 0, 0, 6, 8, 0, 0, 0, 0 }, { 0, 2, 8, 0, 0, 0, 0, 0, 0 } };
		int[][] result = Solver.solve(unsolved);
		// bad testing sollution since java doesn't implement a proper "equals"
		// for double arrays, we iterate the double array and compare the inner
		// arrays.
		for (int i = 0; i < result.length; i++)
			if (!Arrays.equals(expected[i], result[i]))
				fail("arrays not equal for y position " + i + ".");
		assertTrue(true);
	}
}
