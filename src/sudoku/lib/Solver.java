package sudoku.lib;

/**
 * A class containing methods for solving and validating sudoku's, please notice
 * that this does <b>not</b> include a parser. The board must be in a 9x9
 * int[][] grid.
 * 
 * @author Dennis Hedegaard
 * 
 */
public final class Solver {
	/**
	 * The state of a board, it can be invalid, valid but not full and valid and
	 * full.<br />
	 * An attempt at consolidating the accept and reject part of a standard
	 * backtrack algorithm for solving sudoku 9x9 puzzles.
	 * 
	 * @author Dennis Hedegaard
	 * 
	 */
	public enum BOARD_STATE {
		/**
		 * The invalid state, the board might be filled or unfilled.
		 */
		INVALID,
		/**
		 * The valid state for an unsolved (not all positions taken) board.
		 */
		UNSOLVED_VALID,
		/**
		 * The valid state for a solved board.
		 */
		SOLVED_VALID,
	}

	/**
	 * Don't allow instances of this class.
	 */
	private Solver() {
	}

	/**
	 * Performs a deep clone on the board supplied, this means no references to
	 * data in the origBoard.
	 * 
	 * @param origBoard
	 *            The original board to clone.
	 * @return A clone of the original board.
	 */
	private static int[][] deepCloneBoard(int[][] origBoard) {
		// make the new objects
		int[][] board = new int[][] { new int[9], new int[9], new int[9],
				new int[9], new int[9], new int[9], new int[9], new int[9],
				new int[9], };
		// copy the integer data
		for (int a = 0; a < origBoard.length; a++)
			System.arraycopy(origBoard[a], 0, board[a], 0, origBoard.length);
		// return the new clone
		return board;
	}

	/**
	 * Checks to see if the board is valid.
	 * 
	 * @param board
	 *            The board to check.
	 * @return Returns {@link BOARD_STATE#INVALID} if the board is invalid,
	 *         {@link BOARD_STATE#UNSOLVED_VALID} if the board is still valid
	 *         but not filled. Returns {@link BOARD_STATE#SOLVED_VALID} if the
	 *         board is filled and valid.
	 * @throws ArrayIndexOutOfBoundsException
	 *             This method does not check boundaries, this is thrown from
	 *             below.
	 */
	@Deprecated
	public static BOARD_STATE check_board(int[][] board) {
		boolean zero = false;
		// check horizontal/vertical
		for (int i = 0; i < 9; i++) {
			// check horizontal
			BOARD_STATE result = check_board_delta(board[i][0], board[i][1],
					board[i][2], board[i][3], board[i][4], board[i][5],
					board[i][6], board[i][7], board[i][8]);
			// if we're in an invalid state now, don't proceed.
			if (result.equals(BOARD_STATE.INVALID))
				return BOARD_STATE.INVALID;
			// check vertical
			result = check_board_delta(board[0][i], board[1][i], board[2][i],
					board[3][i], board[4][i], board[5][i], board[6][i],
					board[7][i], board[8][i]);
			// check the result, if solved and valid change nothing.
			switch (result) {
			case INVALID:
				return BOARD_STATE.INVALID;
			case UNSOLVED_VALID:
				zero = true;
				break;
			}
		}
		// check zones of 3x3 grids in the 9x9 grid.
		for (int y = 0; y < 3; y++)
			for (int x = 0; x < 3; x++)
				if (check_board_delta(board[x * 3][y * 3],
						board[x * 3 + 1][y * 3], board[x * 3 + 2][y * 3],
						board[x * 3][y * 3 + 1], board[x * 3 + 1][y * 3 + 1],
						board[x * 3 + 2][y * 3 + 1], board[x * 3][y * 3 + 2],
						board[x * 3 + 1][y * 3 + 2],
						board[x * 3 + 2][y * 3 + 2])
						.equals(BOARD_STATE.INVALID))
					return BOARD_STATE.INVALID;
		// At this point the board is valid, however it might not be solved.
		if (zero)
			return BOARD_STATE.UNSOLVED_VALID;
		else
			return BOARD_STATE.SOLVED_VALID;
	}

	/**
	 * Checks a row/column/zone of a board, it returns the same states as the
	 * {@link #check_board(int[][])} method.
	 * 
	 * @param row
	 *            The delta to check, this algorithm only checks 0..8 (length
	 *            9).
	 * @return If the board is valid, but contains 0's returns
	 *         {@link BOARD_STATE#UNSOLVED_VALID}, if the board is valid and
	 *         doesn't contain 0's returns {@link BOARD_STATE#SOLVED_VALID} or
	 *         if the delta isn't valid {@link BOARD_STATE#INVALID}.
	 */
	public static BOARD_STATE check_board_delta(int... delta) {
		// contains a bitmap of wether a digit has been encountered already.
		boolean[] digits = new boolean[9];
		// ends up true if the board is unsolved.
		boolean zero = false;
		// first we iterate the digits.
		for (int i : delta) {
			if (i == 0) // A zero means an empty stop.
				zero = true;
			else if (digits[i - 1] == true) // this means we've hit the same
											// non-zero number twice.
				return BOARD_STATE.INVALID;
			else
				// we've hit a new number, mark it in the array.
				digits[i - 1] = true;
		}
		// TODO: Perhaps check for only true values in the digits-array ?
		// return the result. At this point it must be valid.
		if (zero)
			return BOARD_STATE.UNSOLVED_VALID;
		else
			return BOARD_STATE.SOLVED_VALID;
	}

	// from the sid branch
	private static boolean isValid(int[][] board, int value, int xpos, int ypos) {
		// check horizontally for the value on all but xpos spots.
		for (int x = 0; x < 9; x++)
			if (xpos != x)
				if (board[ypos][x] == value)
					return false;
		// check vertically for the value on all but ypos spots.
		for (int y = 0; y < 9; y++)
			if (ypos != y)
				if (board[y][xpos] == value)
					return false;
		// check the 3x3 box.
		int xbox = ((int) xpos / 3) * 3;
		int ybox = ((int) ypos / 3) * 3;
		for (int x = xbox; x < xbox + 2; x++)
			for (int y = ybox; y < ybox + 2; y++)
				if (ypos != y && xpos != x)
					if (board[y][x] == value)
						return false;
		// all seems fine, return true
		return true;
	}

	/**
	 * A facade for calling the backtrack method.
	 * 
	 * @param board
	 *            The board to solve.
	 * @return The sollution if any, else <code>null</code> is returned.
	 */
	public static int[][] solve(int[][] board) {
		return backtrack(board, 0, 0);
	}

	/**
	 * A classic implementation of a backtrack algorithm, used for solving
	 * sudokus.
	 * 
	 * @param board
	 *            The board in a 9x9 grid.
	 * @param xpos
	 *            The current x position in the board to check and brute force.
	 * @param ypos
	 *            The current y position in the board to check and brute force.
	 * @return Returns int[][] if a valid sollution was found, otherwise
	 *         <code>null</code>.
	 */
	public static int[][] backtrack(int[][] board, int xpos, int ypos) {
		// first perform a deep clone.
		board = deepCloneBoard(board);
		if (board[ypos][xpos] != 0) { // proceed on non-free positions.
			return next(board, xpos, ypos);
		} else {
			for (int i = 1; i <= 9; i++) { // brute force and proceed.
				if (!isValid(board, i, xpos, ypos))
					continue;
				board[ypos][xpos] = i;
				int[][] result = next(board, xpos, ypos);
				if (result != null)
					return result;
			}
			return null;
		}
	}

	/**
	 * Find the next move for a indirect recursive call to
	 * {@link #backtrack(int[][], int, int)} or die.
	 * 
	 * @param board
	 *            The board to pass on
	 * @param xpos
	 *            The x position, if this is 8, increment y and set x to 0.
	 * @param ypos
	 * @return
	 */
	private static int[][] next(int[][] board, int xpos, int ypos) {
		if (xpos == 8)
			if (ypos == 8)
				return board;
			else
				return backtrack(board, 0, ypos + 1);
		else
			return backtrack(board, xpos + 1, ypos);
	}
}
